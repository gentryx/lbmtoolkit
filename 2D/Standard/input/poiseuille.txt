sizeX 153
sizeY 51
timesteps 30000
dt 0.001
dx 0.001
tau 0.9
GravityX 0.0001
GravityY 0.0
ForceX 0.0
ForceY 0.0
################### INIT
initPressure 1.0
initVelocityX_L 0.0
initVelocityY_L 0.0

################### OUTPUT
OutputVelocity test0001
OutputDensity NULL
OutputFlag NULL

printstep 1000
tracestep 1000

################### BOUNDARY
North  ZOU_HE_NORTH_VELOCITY 0.0 0.0 1.0
South  ZOU_HE_SOUTH_VELOCITY 0.0 0.0 1.0
East   FLUID
West   FLUID

################### CORNERS
NorthEast ZOU_HE_NORTH_VELOCITY 0.0 0.0 1.0
NorthWest ZOU_HE_NORTH_VELOCITY 0.0 0.0 1.0
SouthEast ZOU_HE_SOUTH_VELOCITY 0.0 0.0 1.0
SouthWest ZOU_HE_SOUTH_VELOCITY 0.0 0.0 1.0

################### IMAGE
Image NULL

################### OBSTACLES

