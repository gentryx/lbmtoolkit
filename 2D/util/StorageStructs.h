#ifndef _STORAGE_STRUCTS_H_
#define _STORAGE_STRUCTS_H_

#include<string>

struct BoundaryStorage{
    std::string type;
    double velocityX;
    double velocityY;
    double pressure;
};

struct TriangleStorage{
    double P1_X;
    double P1_Y;
    double P2_X;
    double P2_Y;
    double P3_X;
    double P3_Y;
};

struct RectangleStorage{
    double M_X;
    double M_Y;
    double l_X;
    double l_Y;
};

struct CircleStorage{
    double M_X;
    double M_Y;
    double r;
};
#endif

