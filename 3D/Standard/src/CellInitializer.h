#ifndef _CELLINITIALIZER_H_
#define _CELLINITIALIZER_H_

#include "Sphere.h"
#include "Cylinder.h"
#include "Box.h"
#include "EnumParser.h"
#include "Cell.h"
#include "Constants.h"
#include "StorageStructs.h"
#include <map>

class CellInitializer : public SimpleInitializer<Cell>
{
private:
    map<string,string> m_boundary;
    vector<Geometry*> m_obstacle;

    int CheckBounceBack( string input, int bounceback ){
        map<string,string>::iterator iter;
        iter = m_boundary.find(input);
        if(bounceback == 1) return 1;
        else if( iter->second == "NO_SLIP" ) return 1;
        else if( iter->second == "FREE_SLIP_T_B" ) return 1;
        else if( iter->second == "FREE_SLIP_N_S" ) return 1;
        else if( iter->second == "FREE_SLIP_E_W" ) return 1;
        else if( iter->second == "ACCELERATION"  ) return 1;
        else return 0;  
    }

    int CheckObstacleBound(int x, int y, int z){
        for(std::vector<Geometry*>::iterator it=m_obstacle.begin(); it != m_obstacle.end();++it){
            if((*it)->IsBoundingBox(x,y,z) == 1){
                 return 1;
            }
        }

        return 0;
    }

    bool CheckObstaclePoint(int x, int y, int z){

        for(std::vector<Geometry*>::iterator it=m_obstacle.begin(); it != m_obstacle.end();++it){
            if((*it)->IsInside(x,y,z) == 1){ 
                return 1;
            }
        }

        return 0;
       
    }

public:
    CellInitializer(Coord<3> dim, int maxSteps) : SimpleInitializer<Cell>(dim, maxSteps)
    {
        m_obstacle.clear();
        m_boundary.clear();
    }

    void SetBoundary (const map<string,string> boundary )
    {
        m_boundary = boundary;
    }

    void SetSphere(int mx, int my, int mz, int r)
    {
         m_obstacle.push_back( new Sphere(mx,my,mz,r) );
    }
    void SetBox(int mx, int my, int mz, int lx, int ly, int lz)
    {
         m_obstacle.push_back( new Box(mx, my,mz, lx,ly,lz) );
    }

    void SetCylinder(int p1x, int p1y, int p1z, int p2x, int p2y, int p2z, int r, int h)
    {
         m_obstacle.push_back( new Cylinder(p1x,p1y,p1z,p2x,p2y,p2z,r,h) );
    }

    virtual void grid(GridBase<Cell, 3> *ret)
    {
        CoordBox<3> box = ret->boundingBox();
        Coord<3> size = this->gridDimensions();
        EnumParser<State> parser;
        map<string,string>::iterator it;

        int xE = size.x()-1;
        int yE = size.y()-1;
        int zE = size.z()-1;
        for (int z = 0; z < size.z(); ++z) {
            for (int y = 0; y < size.y(); ++y) {
                for (int x = 0; x < size.x(); ++x) {
                    Coord<3> c(x, y, z);
                    State s = FLUID;
                    int bb = 0;
                    if (c.z() == 0 ) {
                        it = m_boundary.find("Bottom");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.z() == zE) {
                        it = m_boundary.find("Top");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.y() == 0 ){
                        it = m_boundary.find("South");
                        s  = parser.ParseEnum( it->second );
                    } 
                    if (c.y() == yE){
                        it = m_boundary.find("North");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.x() == 0 ){
                        it = m_boundary.find("West");
                        s  = parser.ParseEnum( it->second );
                    }
                    if (c.x() == xE){
                        it = m_boundary.find("East");
                        s  = parser.ParseEnum( it->second );
                    }

                    
                    // ===== Edges ======
                    if( c.z() == zE && c.y() == yE ){
                        it = m_boundary.find("TopNorth");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.y() == 0 ){
                        it = m_boundary.find("TopSouth");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.x() == xE ){
                        it = m_boundary.find("TopEast");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.x() == 0 ){
                        it = m_boundary.find("TopWest");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0 && c.y() == yE ){
                        it = m_boundary.find("BottomNorth");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0 && c.y() == 0 ){
                        it = m_boundary.find("BottomSouth");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0 && c.x() == xE ){
                        it = m_boundary.find("BottomEast");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0 && c.x() == 0 ){
                        it = m_boundary.find("BottomWest");
                        s  =parser.ParseEnum( it->second );
                    }
                    if( c.y() == yE && c.x() == xE ){
                        it = m_boundary.find("NorthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.y() == yE && c.x() == 0 ){
                        it = m_boundary.find("NorthWest");
                        s  = parser.ParseEnum( it->second );
                    }
                    if( c.y() == 0 && c.x() == xE ){
                        it = m_boundary.find("SouthEast");
                        s  = parser.ParseEnum( it->second );
                    } 
                    if( c.y() == 0 && c.x() == 0  ){
                        it = m_boundary.find("SouthWest");
                        s  = parser.ParseEnum( it->second );
                    }

                    
                    // ===== Corner =====
                    if( c.z() == zE && c.y() == yE && c.x() == xE ){
                        it = m_boundary.find("TopNorthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.y() == yE && c.x() == 0 ){
                        it = m_boundary.find("TopNorthWest");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.y() == 0  && c.x() == xE ){
                        it = m_boundary.find("TopSouthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == zE && c.y() == 0  && c.x() == 0  ){
                        it = m_boundary.find("TopSouthWest");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0  && c.y() == yE && c.x() == xE ){
                        it = m_boundary.find("BottomNorthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0  && c.y() == yE && c.x() == 0  ){
                        it = m_boundary.find("BottomNorthWest");
                        s = parser.ParseEnum( it->second );
                    } 
                    if( c.z() == 0  && c.y() == 0  && c.x() == xE ){
                        it = m_boundary.find("BottomSouthEast");
                        s = parser.ParseEnum( it->second );
                    }
                    if( c.z() == 0  && c.y() == 0  && c.x() == 0  ){
                        it = m_boundary.find("BottomSouthWest");
                        s = parser.ParseEnum( it->second );
                    }

                    //==== Bounceback ====
                    if(c.z() == zE-1){
                        bb = CheckBounceBack("Top", bb);
                    } 
                    if(c.z() == 1   ){
                        bb = CheckBounceBack("Bottom", bb );
                    } 
                    if(c.y() == yE-1){
                        bb = CheckBounceBack("North", bb);
                    } 
                    if(c.y() == 1   ){
                        bb = CheckBounceBack("South", bb);
                    } 
                    if(c.x() == xE-1){
                        bb = CheckBounceBack("East", bb);
                    } 
                    if(c.x() == 1   ){
                        bb = CheckBounceBack("West", bb);
                    } 


                    if (CheckObstacleBound( c.x(),c.y(),c.z() ) == 1){
                        bb = 1;
                    } 

                    if( CheckObstaclePoint( c.x(),c.y(),c.z() ) == 1){
                        bb = 0;
                        s = OBSTACLE;
                    }

                   if (box.inBounds(c))
                        ret->at(c) = Cell( s,x,y,z,bb);
                }
            }
        }
    }
};



#endif
