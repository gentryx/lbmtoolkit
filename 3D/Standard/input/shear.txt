sizeX 32	
sizeY 32
sizeZ 32
timesteps 40000
dt 0.001
dx 0.001
tau 0.9
GravityX 0.0
GravityY 0.0
GravityZ 0.0
ForceX 0.0
ForceY 0.0
ForceZ 0.0

################## INIT
initPressure 1.0
initVelocityX_L 0.0
initVelocityY_L 0.0
initVelocityZ_L 0.0

################### OUTPUT
OutputVelocity 3D_ShearFlow_T0_9_S0_02_XYZ_32
OutputDensity NULL
OutputFlag NULL
OutputDrag NULL

printstep 5000
tracestep 1000

################### BOUNDARY
Top    ZOU_HE_TOP_VELOCITY 0.02 0.0 0.0 1.0
Bottom ZOU_HE_BOTTOM_VELOCITY -0.02 0.0 0.0 1.0
North  FLUID
South  FLUID
East   FLUID
West   FLUID

################### EDGES
TopNorth     ZOU_HE_TOP_VELOCITY 0.02 0.0 0.0 1.0
TopSouth     ZOU_HE_TOP_VELOCITY 0.02 0.0 0.0 1.0
TopEast      ZOU_HE_TOP_VELOCITY 0.02 0.0 0.0 1.0
TopWest      ZOU_HE_TOP_VELOCITY 0.02 0.0 0.0 1.0
BottomNorth  ZOU_HE_BOTTOM_VELOCITY -0.02 0.0 0.0 1.0
BottomSouth  ZOU_HE_BOTTOM_VELOCITY -0.02 0.0 0.0 1.0
BottomEast   ZOU_HE_BOTTOM_VELOCITY -0.02 0.0 0.0 1.0
BottomWest   ZOU_HE_BOTTOM_VELOCITY -0.02 0.0 0.0 1.0
NorthEast    FLUID
NorthWest    FLUID 
SouthEast    FLUID
SouthWest    FLUID

################### CORNERS
TopNorthEast    ZOU_HE_TOP_VELOCITY
TopNorthWest    ZOU_HE_TOP_VELOCITY
TopSouthEast    ZOU_HE_TOP_VELOCITY
TopSouthWest    ZOU_HE_TOP_VELOCITY
BottomNorthEast ZOU_HE_BOTTOM_VELOCITY
BottomNorthWest ZOU_HE_BOTTOM_VELOCITY
BottomSouthEast ZOU_HE_BOTTOM_VELOCITY
BottomSouthWest ZOU_HE_BOTTOM_VELOCITY

