#ifndef _ENUM_PARSER_H_
#define _ENUM_PARSER_H_

#include <map>
#include <string>
#include <vector>

#include "Constants.h"

using namespace std;

template <typename T>
class EnumParser
{
    map<string, T> enumMap;
public:
    EnumParser(){
        enumMap["FLUID"]                             = FLUID;
        enumMap["NO_SLIP"]                           = NO_SLIP;
        enumMap["OBSTACLE"]                          = OBSTACLE;
        enumMap["FREE_SLIP_E_W"]                     = FREE_SLIP_E_W;
        enumMap["FREE_SLIP_N_S"]                     = FREE_SLIP_N_S;
        enumMap["ZOU_HE_SOUTH_VELOCITY"]             = ZOU_HE_SOUTH_VELOCITY;
        enumMap["ZOU_HE_NORTH_VELOCITY"]             = ZOU_HE_NORTH_VELOCITY;
        enumMap["ZOU_HE_EAST_VELOCITY"]              = ZOU_HE_EAST_VELOCITY;
        enumMap["ZOU_HE_WEST_VELOCITY"]              = ZOU_HE_WEST_VELOCITY;
        enumMap["ZOU_HE_SOUTH_PRESSURE"]             = ZOU_HE_SOUTH_PRESSURE ;
        enumMap["ZOU_HE_NORTH_PRESSURE"]             = ZOU_HE_NORTH_PRESSURE;
        enumMap["ZOU_HE_EAST_PRESSURE"]              = ZOU_HE_EAST_PRESSURE;
        enumMap["ZOU_HE_WEST_PRESSURE"]              = ZOU_HE_WEST_PRESSURE;
        enumMap["ACCELERATION"]                      = ACCELERATION;
        enumMap["CORNER_SOUTH_WEST_VELOCITY"]        = CORNER_SOUTH_WEST_VELOCITY;
        enumMap["CORNER_SOUTH_EAST_VELOCITY"]        = CORNER_SOUTH_EAST_VELOCITY;
        enumMap["CORNER_NORTH_WEST_VELOCITY"]        = CORNER_NORTH_WEST_VELOCITY;
        enumMap["CORNER_NORTH_EAST_VELOCITY"]        = CORNER_NORTH_EAST_VELOCITY;
        enumMap["CORNER_SOUTH_WEST_PRESSURE"]        = CORNER_SOUTH_WEST_PRESSURE;
        enumMap["CORNER_SOUTH_EAST_PRESSURE"]        = CORNER_SOUTH_EAST_PRESSURE;
        enumMap["CORNER_NORTH_WEST_PRESSURE"]        = CORNER_NORTH_WEST_PRESSURE;
        enumMap["CORNER_NORTH_EAST_PRESSURE"]        = CORNER_NORTH_EAST_PRESSURE;
        enumMap["DUMMY"]                             = DUMMY;
    
    }
    T ParseEnum( const string &value )
    {
        typename map<string,T>::const_iterator pos;
        pos = enumMap.find(value);
        if( pos == enumMap.end() ){
           throw runtime_error("ENUM ERROR -- >  "+ value);
        }
        return pos->second;
    }
};

#endif
