#!/bin/bash
echo #JOB_ID
echo #QUEUE
#$ -pe openmpi 4 
#$ -q whistler.long.q
#$ -cwd
#$ -l exclusive=true
#$ -l h_rt=01:00:00
mpirun.openmpi ./main "../input/params.txt"
