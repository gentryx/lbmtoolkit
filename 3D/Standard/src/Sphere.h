#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "Geometry.h"

class Sphere : public Geometry
{
   private:
      double m_RR;
      double m_RRb;

   public:
      Sphere(int mx, int my, int mz, int r ) : Geometry(mx,my,mz)
      {
         m_RR = r*r;
	 m_RRb = (r+2)*(r+2);
      }

      bool IsInside(int x, int y, int z)
      {
         int kX = (x-m_X) * (x-m_X);
         int kY = (y-m_Y) * (y-m_Y);
         int kZ = (z-m_Z) * (z-m_Z);
         int sum = kX+kY+kZ;
         if(sum <= m_RR){
             return 1;
         }
         else{
           return 0;
         }

      }


      bool IsBoundingBox(int x, int y, int z)
      {
         int kX = (x-m_X) * (x-m_X);
         int kY = (y-m_Y) * (y-m_Y);
         int kZ = (z-m_Z) * (z-m_Z);
         int sum = kX+kY+kZ;
         if(sum <= m_RRb){
             return 1;
         }
         else{
           return 0;
         }

      }

};

#endif
