#ifndef SELECTORBOV_H_
#define SELECTORBOV_H_

class DensitySelector 
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "density";
    }

    static std::string dataFormat()
    {
        return "DOUBLE";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
        *storage = cell.density;
    }
};

class VelocitySelector 
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "velocity";
    }

    static std::string dataFormat()
    {
        return "DOUBLE";
    }

    static int dataComponents()
    {
        return 3;
    }

    void operator()(const Cell& cell, double *storage)
    {
        storage[0] = cell.velocityX;
        storage[1] = cell.velocityY;
        storage[2] = cell.velocityZ;
    }
};

class FlagSelector 
{
public:
    typedef int VariableType;

    static std::string varName()
    {
        return "flag";
    }

    static std::string dataFormat()
    {
        return "INT";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, int *storage)
    {
        storage[0] = (int)cell.state;
    }
};

#endif
