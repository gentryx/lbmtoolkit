sizeX  600
sizeY  200
timesteps 50000
dt 0.001
dx 0.001
tau 0.58
GravityX 0.0
GravityY 0.0
ForceX 0.0
ForceY 0.0
################### INIT
initPressure 1.0
initVelocityX_L 0.0
initVelocityY_L 0.0

################### OUTPUT
OutputVelocity Karman
OutputDensity NULL
OutputFlag NULL

printstep 50
tracestep 10000

################### BOUNDARY
North  NO_SLIP
South  NO_SLIP
East   ZOU_HE_EAST_PRESSURE 0.0 0.0 1.0
West   ZOU_HE_WEST_VELOCITY 0.08 0.0 1.0

################### CORNERS
NorthEast NO_SLIP
NorthWest NO_SLIP
SouthEast NO_SLIP
SouthWest NO_SLIP

################### IMAGE
Image NULL

################### OBSTACLES
# Rectangle 100 120 5 5
 Circle 100 120 20

