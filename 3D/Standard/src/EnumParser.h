#ifndef _ENUM_PARSER_H_
#define _ENUM_PARSER_H_

#include <map>
#include <string>
#include <vector>

#include "Constants.h"

using namespace std;

template <typename T>
class EnumParser
{
    map<string, T> enumMap;
public:
    EnumParser(){
        enumMap["FLUID"]                             = FLUID;
        enumMap["NO_SLIP"]                           = NO_SLIP;
        enumMap["OBSTACLE"]                          = OBSTACLE;
        enumMap["FREE_SLIP_E_W"]                     = FREE_SLIP_E_W;
        enumMap["FREE_SLIP_N_S"]                     = FREE_SLIP_N_S;
        enumMap["FREE_SLIP_T_B"]                     = FREE_SLIP_T_B;
        enumMap["ZOU_HE_BOTTOM_VELOCITY"]            = ZOU_HE_BOTTOM_VELOCITY;
        enumMap["ZOU_HE_TOP_VELOCITY"]               = ZOU_HE_TOP_VELOCITY;
        enumMap["ZOU_HE_SOUTH_VELOCITY"]             = ZOU_HE_SOUTH_VELOCITY;
        enumMap["ZOU_HE_NORTH_VELOCITY"]             = ZOU_HE_NORTH_VELOCITY;
        enumMap["ZOU_HE_EAST_VELOCITY"]              = ZOU_HE_EAST_VELOCITY;
        enumMap["ZOU_HE_WEST_VELOCITY"]              = ZOU_HE_WEST_VELOCITY;
        enumMap["ZOU_HE_BOTTOM_PRESSURE"]            = ZOU_HE_BOTTOM_PRESSURE;
        enumMap["ZOU_HE_TOP_PRESSURE"]               = ZOU_HE_TOP_PRESSURE;
        enumMap["ZOU_HE_SOUTH_PRESSURE"]             = ZOU_HE_SOUTH_PRESSURE ;
        enumMap["ZOU_HE_NORTH_PRESSURE"]             = ZOU_HE_NORTH_PRESSURE;
        enumMap["ZOU_HE_EAST_PRESSURE"]              = ZOU_HE_EAST_PRESSURE;
        enumMap["ZOU_HE_WEST_PRESSURE"]              = ZOU_HE_WEST_PRESSURE;
        enumMap["ACCELERATION"]                      = ACCELERATION;
        enumMap["CORNER_BOTTOM_SOUTH_WEST_VELOCITY"] = CORNER_BOTTOM_SOUTH_WEST_VELOCITY;
        enumMap["CORNER_BOTTOM_SOUTH_EAST_VELOCITY"] = CORNER_BOTTOM_SOUTH_EAST_VELOCITY;
        enumMap["CORNER_BOTTOM_NORTH_WEST_VELOCITY"] = CORNER_BOTTOM_NORTH_WEST_VELOCITY;
        enumMap["CORNER_BOTTOM_NORTH_EAST_VELOCITY"] = CORNER_BOTTOM_NORTH_EAST_VELOCITY;
        enumMap["CORNER_TOP_SOUTH_WEST_VELOCITY"]    = CORNER_TOP_SOUTH_WEST_VELOCITY;
        enumMap["CORNER_TOP_SOUTH_EAST_VELOCITY"]    = CORNER_TOP_SOUTH_EAST_VELOCITY;
        enumMap["CORNER_TOP_NORTH_WEST_VELOCITY"]    = CORNER_TOP_NORTH_WEST_VELOCITY;
        enumMap["CORNER_TOP_NORTH_EAST_VELOCITY"]    = CORNER_TOP_NORTH_EAST_VELOCITY;
        enumMap["CORNER_BOTTOM_SOUTH_WEST_PRESSURE"] = CORNER_BOTTOM_SOUTH_WEST_PRESSURE;
        enumMap["CORNER_BOTTOM_SOUTH_EAST_PRESSURE"] = CORNER_BOTTOM_SOUTH_EAST_PRESSURE;
        enumMap["CORNER_BOTTOM_NORTH_WEST_PRESSURE"] = CORNER_BOTTOM_NORTH_WEST_PRESSURE;
        enumMap["CORNER_BOTTOM_NORTH_EAST_PRESSURE"] = CORNER_BOTTOM_NORTH_EAST_PRESSURE;
        enumMap["CORNER_TOP_SOUTH_WEST_PRESSURE"]    = CORNER_TOP_SOUTH_WEST_PRESSURE;
        enumMap["CORNER_TOP_SOUTH_EAST_PRESSURE"]    = CORNER_TOP_SOUTH_EAST_PRESSURE;
        enumMap["CORNER_TOP_NORTH_WEST_PRESSURE"]    = CORNER_TOP_NORTH_WEST_PRESSURE;
        enumMap["CORNER_TOP_NORTH_EAST_PRESSURE"]    = CORNER_TOP_NORTH_EAST_PRESSURE;
        enumMap["EDGE_BOTTOM_WEST_VELOCITY"]         =  EDGE_BOTTOM_WEST_VELOCITY;
        enumMap["EDGE_BOTTOM_EAST_VELOCITY"]         =  EDGE_BOTTOM_EAST_VELOCITY;
        enumMap["EDGE_BOTTOM_SOUTH_VELOCITY"]        = EDGE_BOTTOM_SOUTH_VELOCITY;
        enumMap["EDGE_BOTTOM_NORTH_VELOCITY"]        = EDGE_BOTTOM_NORTH_VELOCITY;
        enumMap["EDGE_TOP_WEST_VELOCITY"]            =  EDGE_TOP_WEST_VELOCITY;
        enumMap["EDGE_TOP_EAST_VELOCITY"]            =  EDGE_TOP_EAST_VELOCITY;
        enumMap["EDGE_TOP_SOUTH_VELOCITY"]           = EDGE_TOP_SOUTH_VELOCITY;
        enumMap["EDGE_TOP_NORTH_VELOCITY"]           = EDGE_TOP_NORTH_VELOCITY;
        enumMap["EDGE_SOUTH_WEST_VELOCITY"]          = EDGE_SOUTH_WEST_VELOCITY;
        enumMap["EDGE_SOUTH_EAST_VELOCITY"]          = EDGE_SOUTH_EAST_VELOCITY;
        enumMap["EDGE_NORTH_WEST_VELOCITY"]          = EDGE_NORTH_WEST_VELOCITY;
        enumMap["EDGE_NORTH_EAST_VELOCITY"]          = EDGE_NORTH_EAST_VELOCITY;
        enumMap["EDGE_BOTTOM_WEST_PRESSURE"]         =  EDGE_BOTTOM_WEST_PRESSURE;
        enumMap["EDGE_BOTTOM_EAST_PRESSURE"]         =  EDGE_BOTTOM_EAST_PRESSURE;
        enumMap["EDGE_BOTTOM_SOUTH_PRESSURE"]        = EDGE_BOTTOM_SOUTH_PRESSURE;
        enumMap["EDGE_BOTTOM_NORTH_PRESSURE"]        = EDGE_BOTTOM_NORTH_PRESSURE;
        enumMap["EDGE_TOP_WEST_PRESSURE"]            =  EDGE_TOP_WEST_PRESSURE;
        enumMap["EDGE_TOP_EAST_PRESSURE"]            =  EDGE_TOP_EAST_PRESSURE;
        enumMap["EDGE_TOP_SOUTH_PRESSURE"]           = EDGE_TOP_SOUTH_PRESSURE;
        enumMap["EDGE_TOP_NORTH_PRESSURE"]           = EDGE_TOP_NORTH_PRESSURE;
        enumMap["EDGE_SOUTH_WEST_PRESSURE"]          = EDGE_SOUTH_WEST_PRESSURE;
        enumMap["EDGE_SOUTH_EAST_PRESSURE"]          = EDGE_SOUTH_EAST_PRESSURE;
        enumMap["EDGE_NORTH_WEST_PRESSURE"]          = EDGE_NORTH_WEST_PRESSURE;
        enumMap["EDGE_NORTH_EAST_PRESSURE"]          = EDGE_NORTH_EAST_PRESSURE;
        enumMap["PERIODICITY"]              = PERIODICITY;
        enumMap["DUMMY"]                    = DUMMY;
    
    }
    T ParseEnum( const string &value )
    {
        typename map<string,T>::const_iterator pos;
        pos = enumMap.find(value);
        if( pos == enumMap.end() ){
           throw runtime_error("ENUM ERRORi -- >  "+ value);
        }
        return pos->second;
    }
};

#endif
