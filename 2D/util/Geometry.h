#ifndef _GEOMETRIE_H_
#define _GEOMETRIE_H_


class Geometry
{
    protected:
       int m_pX;
       int m_pY;
    
    public:
       Geometry(int mx, int my) : m_pX(mx),m_pY(my){  }       

       virtual bool IsInside(int x, int y){return 0;}
       virtual bool IsBoundingBox(int x, int y){return 0;}

};

bool CheckPoint(Geometry *g, int x, int y)
{
 return g->IsInside(x,y);
}

bool CheckBound(Geometry *g, int x, int y)
{
 return g->IsBoundingBox(x,y);
}

#endif
