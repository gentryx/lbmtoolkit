sizeX 64
sizeY 32
timesteps 50000
dt 0.001
dx 0.001
tau 1.3
GravityX 0.0
GravityY 0.0
ForceX 0.0
ForceY 0.0
################### INIT
initPressure 1.0
initVelocityX_L 0.0
initVelocityY_L 0.0

################### OUTPUT
OutputVelocity test2_NOSLIP
OutputDensity NULL
OutputFlag NULL

printstep 50000
tracestep 1000

################### BOUNDARY
North  NO_SLIP
South  NO_SLIP
East   ZOU_HE_EAST_PRESSURE 0.0 0.0 1.0
West   ZOU_HE_WEST_PRESSURE 0.0 0.0 1.02


################### CORNERS
NorthEast NO_SLIP
NorthWest NO_SLIP
SouthEast NO_SLIP
SouthWest NO_SLIP

################### IMAGE
Image NULL 

################### OBSTACLES

