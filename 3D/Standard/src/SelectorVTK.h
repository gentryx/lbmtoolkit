#ifndef SELECTORVTK_H_
#define SELECTORVTK_H_

class DensitySelector 
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "density";
    }

    static std::string dataFormat()
    {
        return "double 1";
    }
    
    static std::string dataType()
    {
        return "SCALARS";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
        *storage = cell.density;
    }
};

class VelocitySelector 
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "velocity";
    }

    static std::string dataFormat()
    {
        return "double";
    }
    
    static std::string dataType()
    {
        return "VECTORS";
    }

    static int dataComponents()
    {
        return 3;
    }

    void operator()(const Cell& cell, double *storage)
    {
        storage[0] = cell.velocityX;
        storage[1] = cell.velocityY;
        storage[2] = cell.velocityZ;
    }
};

class FlagSelector
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "state";
    }

    static std::string dataFormat()
    {
        return "int";
    }

    static std::string dataType()
    {
        return "SCALAR";
    }

    static int dataComponents()
    {
        return 1;
    }

    void operator()(const Cell& cell, double *storage)
    {
        storage[0] = cell.state;
    }
};


#endif
