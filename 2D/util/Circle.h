#ifndef _CIRCLE_H_
#define _CIRCLE_H_

#include "Geometry.h"

class Circle : public Geometry
{
   private:
      double m_RR;
      double m_RRb;

   public:
      Circle(int mx, int my, int r ) : Geometry(mx,my)
      {
         m_RR = r*r;
	 m_RRb = (r+2)*(r+2);
      }

      bool IsInside(int x, int y)
      {
         int kX = (x-m_pX) * (x-m_pX);
         int kY = (y-m_pY) * (y-m_pY);
         int sum = kX+kY;
         if(sum <= m_RR){
             return 1;
         }
         else{
           return 0;
         }

      }


      bool IsBoundingBox(int x, int y)
      {
         int kX = (x-m_pX) * (x-m_pX);
         int kY = (y-m_pY) * (y-m_pY);
         int sum = kX+kY;
         if(sum <= m_RRb){
             return 1;
         }
         else{
           return 0;
         }

      }

};

#endif
