#include <libgeodecomp/io/asciiwriter.h>
#include <libgeodecomp/io/simpleinitializer.h>
#include <libgeodecomp/io/tracingwriter.h>
#include <libgeodecomp/parallelization/serialsimulator.h>

#include "../src/Cell.h"
#include "../src/Provider.h"
#include "../src/CellInitializer.h"
#include "../src/SelectorVTK.h"
#include "../src/FileReader.h"
#include "../src/Constants.h"
#include "../src/VTKwriter.h"

using namespace LibGeoDecomp;


void SetBoundaries(FileReader* Reader, CellInitializer* cellinit){
    std::map<string,string> boundary;
    
    boundary["Top"]    = Reader->GetType("Top") ;
    boundary["Bottom"] = Reader->GetType("Bottom") ;
    boundary["North"]  = Reader->GetType("North") ;
    boundary["South"]  = Reader->GetType("South") ;
    boundary["East"]   = Reader->GetType("East") ;
    boundary["West"]   = Reader->GetType("West") ;

    boundary["TopNorth"]    = Reader->GetType("TopNorth") ;
    boundary["TopSouth"]    = Reader->GetType("TopSouth") ;
    boundary["TopEast"]     = Reader->GetType("TopEast") ;
    boundary["TopWest"]     = Reader->GetType("TopWest") ;
    boundary["BottomNorth"] = Reader->GetType("BottomNorth") ;
    boundary["BottomSouth"] = Reader->GetType("BottomSouth") ;
    boundary["BottomEast"]  = Reader->GetType("BottomEast") ;
    boundary["BottomWest"]  = Reader->GetType("BottomWest") ;
    boundary["NorthEast"]   = Reader->GetType("NorthEast") ;
    boundary["NorthWest"]   = Reader->GetType("NorthWest") ;
    boundary["SouthEast"]   = Reader->GetType("SouthEast") ;
    boundary["SouthWest"]   = Reader->GetType("SouthWest"); 
    
    boundary["TopNorthEast"]    = Reader->GetType("TopNorthEast") ;
    boundary["TopNorthWest"]    = Reader->GetType("TopNorthWest") ;
    boundary["TopSouthEast"]    = Reader->GetType("TopSouthEast") ;
    boundary["TopSouthWest"]    = Reader->GetType("TopSouthWest") ;
    boundary["BottomNorthEast"] = Reader->GetType("BottomNorthEast") ;
    boundary["BottomNorthWest"] = Reader->GetType("BottomNorthWest") ;
    boundary["BottomSouthEast"] = Reader->GetType("BottomSouthEast") ;
    boundary["BottomSouthWest"] = Reader->GetType("BottomSouthWest") ;

    cellinit->SetBoundary(boundary);

}


void SetObstacles( CellInitializer* cellinit, FileReader* Reader ){

    vector<BoxStorage> obstacleBox = Reader->GetBoxes();
    vector<SphereStorage> obstacleSphere = Reader->GetSpheres();
    vector<CylinderStorage> obstacleCylinder = Reader->GetCylinders();

    std::vector<BoxStorage>::iterator itBox;
    for( itBox=obstacleBox.begin(); itBox != obstacleBox.end(); ++itBox){
        cellinit->SetBox(itBox->M_X, itBox->M_Y, itBox->M_Z, itBox->l_X, itBox->l_Y, itBox->l_Z);
    }
  
    std::vector<CylinderStorage>::iterator itCyl;
    for( itCyl=obstacleCylinder.begin(); itCyl != obstacleCylinder.end(); ++itCyl ){
        cellinit->SetCylinder(itCyl->P1_X, itCyl->P1_Y, itCyl->P1_Z, itCyl->P2_X, itCyl->P2_Y, itCyl->P2_Z, itCyl->h, itCyl->r );
    }

    std::vector<SphereStorage>::iterator itSph;
    for( itSph=obstacleSphere.begin(); itSph != obstacleSphere.end(); ++itSph ){
        cellinit->SetSphere(itSph->M_X, itSph->M_Y, itSph->M_Z, itSph->r  );
    }

}

void SetWriter(SerialSimulator<Cell>* sim, FileReader* Reader)
{

    const string outputvelocity = Reader->GetValue<string>("OutputVelocity");
    const string outputdensity  = Reader->GetValue<string>("OutputDensity");
    const string outputflag     = Reader->GetValue<string>("OutputFlag");    
    const string outputdrag     = Reader->GetValue<string>("OutputDrag");

    const int printstep         = Reader->GetValue<int>("printstep");
    const int tracestep         = Reader->GetValue<int>("tracestep");
    const int timesteps         = Reader->GetValue<int>("timesteps"); 

    if(outputvelocity != "NULL" ){
        sim->addWriter( new VTKWriter<Cell, VelocitySelector>(outputvelocity+".velocity",printstep) );
    }
    if(outputdensity != "NULL" ){
        sim->addWriter( new VTKWriter<Cell, DensitySelector>(outputdensity+".density",printstep) );
    }
    if(outputflag != "NULL" ){
        sim->addWriter( new VTKWriter<Cell, FlagSelector>(outputflag+".flag",printstep) );

    }
//    if(outputdrag != "NULL" ){
//        sim->addWriter( new DragForceWriter<Cell, DragForceSelector>(outputdrag+".drag",printstep) );
//    }

    sim->addWriter( new TracingWriter<Cell>( tracestep, timesteps ) );

}

void runSimulation(CellInitializer* cellinit, FileReader* Reader)
{

    SerialSimulator<Cell> sim( cellinit );
    SetWriter(&sim, Reader );
    sim .run();

}
int main(int argc, char *argv[])
{
    if(argc != 2){
        cerr << "Missing Parameter File!" << endl;
        exit(1);
    }

    FileReader Reader;
    Reader.read( argv[1] );
    const int sizeX     = Reader.GetValue<int>("sizeX");
    const int sizeY     = Reader.GetValue<int>("sizeY");
    const int sizeZ     = Reader.GetValue<int>("sizeZ");
    const int timesteps = Reader.GetValue<int>("timesteps");

    CellInitializer *cellinit = new CellInitializer( Coord<3>(sizeX,sizeY,sizeZ), timesteps );
    SetBoundaries(&Reader,cellinit);
    SetObstacles(cellinit, &Reader);

    runSimulation(cellinit, & Reader);

//    SerialSimulator<Cell> sim( cellinit );
//    SetWriter(&sim, &Reader );
//    sim .run();

    return 0;
}


