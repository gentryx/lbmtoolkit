#ifndef _FILEREADER_H_
#define _FILEREADER_H_

#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <sstream>

#include "StorageStructs.h"

using namespace std;

class FileReader{

private:
    map<string,string> m_tablet;
    map<string,BoundaryStorage> m_tablet_2;
    vector<BoxStorage> m_storageBox;
    vector<SphereStorage> m_storageSphere;
    vector<CylinderStorage> m_storageCylinder;
public:
    void read(string in);
    void PrintError(string message);

    BoundaryStorage GetStruct(const string key);
    template<typename T>
    T GetValue(const string key);
    string GetType(const string key);
    
    vector<BoxStorage>       GetBoxes();
    vector<CylinderStorage>  GetCylinders();
    vector<SphereStorage>    GetSpheres();
  
};

inline void FileReader::read(string f_in)
{
    char line[80];
    string first,second;
    double ux, uy, uz, p;
    ifstream input;
    input.open(f_in.c_str(), ifstream::in);
    
    if(input.is_open()){

        while( input.getline(line,80)){
            if(strlen(line) == 0 || line[0] == '#') continue;
            istringstream istr(line);
            if (istr >> first ){
                if(first.find("North") != string::npos || first.find("South") != string::npos || first.find("East")!=string::npos || first.find("West")!=string::npos || first.find("Top")!=string::npos || first.find("Bottom")!=string::npos ){
                    if(istr >> second >> ux >> uy >> uz >> p){
                        BoundaryStorage tmp = {second,ux,uy,uz,p};
                        m_tablet_2[first.c_str()] = tmp; 
                    }
                    else{
                        BoundaryStorage tmp2 = {second,0.0,0.0,1.0};
                        m_tablet_2[first.c_str()] = tmp2;
                    }
                }
                else if (first == "Cylinder"){
                    double P1_x, P1_y, P1_z, P2_x, P2_y, P2_z, h, r;
                    if(istr >> P1_x >> P1_y >> P1_z >> P2_x >> P2_y >> P2_z >> h >> r){
                        CylinderStorage tmpCyl = {P1_x, P1_y, P1_z, P2_x, P2_y, P2_z, h, r};
                        m_storageCylinder.push_back(tmpCyl);        
                    }
                    else{
                        PrintError("Cylinder wrong ");
                    }
                }
                else if(first == "Box"){
                    double midBox_x, midBox_y, midBox_z, l_x, l_y, l_z;
                    if(istr >> midBox_x >> midBox_y >> midBox_z >> l_x >> l_y >> l_z ){
                        BoxStorage tmpBox = {midBox_x, midBox_y, midBox_z, l_x, l_y, l_z};
                        m_storageBox.push_back(tmpBox); 
                    }
                    else{
                        PrintError("Box wrong ");
                    }
                }
                else if(first == "Sphere"){
                    double midSph_x, midSph_y, midSph_z, r;
                    if(istr >> midSph_x >> midSph_y >> midSph_z >> r){
                        SphereStorage tmpSph = {midSph_x, midSph_y, midSph_z, r};
                        m_storageSphere.push_back(tmpSph);
                    }
                    else{
                        PrintError("Sphere wrong");
                    }
                } 
                else{
                    if(istr >> second){
                        m_tablet[first.c_str()] = second.c_str();
                    }
                    else{
			std::cout << "second " << std::endl;
                        PrintError(first);
                    }
                }
            }
            else{
                std::cout << "error " << first << endl;
                PrintError(first);
            }
        
        }
    }
    else{
        PrintError("OpeningFile");
    }
    input.close();
}

inline void FileReader::PrintError(string message)
{
    cerr << "ERROR: Parameterfile -> " << message << endl;
    exit(1);
}

inline BoundaryStorage FileReader::GetStruct(const string key )
{
    map<string,BoundaryStorage>::iterator it;

    it = m_tablet_2.find(key);
    if(it == m_tablet_2.end()){
        PrintError(key);
    }
    return it->second;
}

inline string FileReader::GetType(const string key)
{
    map<string,BoundaryStorage>::iterator it;
    it = m_tablet_2.find(key);
    if(it == m_tablet_2.end()){
        std::cout << key <<std::endl;
        PrintError(key);
    }
    return it->second.type;
}

template<typename T>
T FileReader::GetValue( const string key )
{
    T val;
    map<string,string>::iterator it;
  
    it=m_tablet.find(key);
    if (it == m_tablet.end()) {
        cerr << "Parameter File not correct Missing / False  Value: " << key << endl;
        exit(0);
    }
    stringstream convert(it->second);
    convert >> val;
    return val;
}

inline vector<BoxStorage> FileReader::GetBoxes()
{
    return m_storageBox;
}

inline vector<SphereStorage> FileReader::GetSpheres()
{
    return m_storageSphere;
}

inline vector<CylinderStorage> FileReader::GetCylinders()
{
   return m_storageCylinder;
}

#endif
