#ifndef _CONVERTER_H_
#define _CONVERTER_H_

#include"FileReader.h"
#include"Constants.h"
#include <vector>

class Provider
{
private:
    double m_dx;
    double m_dt;
    double m_tau; 
    
    double m_nueP; // Si Value
    double m_nueL; // Lattice Value
    
    
    int m_timesteps;
    int m_size[3]; //TODO: Dimension als Template?

    double m_pressureIn;
    double m_pressureOut;
	
    double m_initPressure;
    double m_initVelocityX; 
    double m_initVelocityY;
    double m_initVelocityZ;

    double m_gravityX;
    double m_gravityY;
    double m_gravityZ;
    double m_forceX;
    double m_forceY;
    double m_forceZ;
  
    map<std::string,BoundaryStorage>* m_boundaryVals;
public:
    Provider();
    ~Provider();
    
//=====================     
//  Access functions:
//=====================

    double GetDx();
    double GetDt();
    double GetTau();
    
    double GetNue_L();
    double GetNue_P();

    double GetGravityX();
    double GetGravityY();
    double GetGravityZ();

    double GetForceX();
    double GetForceY();
    double GetForceZ();
    
    double GetVelocityX( string key );
    double GetTypeVelocityX( string type );

    double GetVelocityY( string key );
    double GetTypeVelocityY( string type );

    double GetVelocityZ( string key );
    double GetTypeVelocityZ( string type );

    double GetPressure( string key);
    double GetTypePressure( string type );

    double GetInitVelocityX();
    double GetInitVelocityY();
    double GetInitVelocityZ();
    double GetInitPressure();	

    int GetSize( char c );    
    int GetTimesteps();

//==================== 	
//  Util stuff Pt 1:
//====================

    double GetSimulationTime();
    double GetSimulationLengthX();
    double GetSimulationLengthY();
    double GetSimulationLengthZ();   

    double CalcOmega();  

private:
    void InitValues();
    
//====================
//   Util stuff Pt 2
//====================
    void CalcNue_L();
    void CalcNue_P();
    
};

#endif
