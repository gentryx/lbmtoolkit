add_executable(2d_incompressible_parallel main.cpp ../src/Cell.cpp ../../util/Provider.cpp ../../util/ImageReader.cpp)
target_link_libraries(2d_incompressible_parallel geodecomp)
