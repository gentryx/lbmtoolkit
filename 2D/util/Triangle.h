#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_


#include "Geometry.h"

class Triangle : public Geometry
{
    private:
        int m_v0X;
        int m_v0Y;
        int m_v1X;
        int m_v1Y;
        int m_midX_B;
        int m_midY_B;
        double m_half_lX_B;
        double m_half_lY_B;
    public:
        Triangle(int P1_X, int P1_Y,int P2_X, int P2_Y, int P3_X, int P3_Y) : Geometry(P1_X,P1_Y)
        {
            m_v0X = P3_X-P1_X;
            m_v0Y = P3_Y-P1_Y;
            m_v1X = P2_X-P1_X;
            m_v1Y = P2_Y-P1_Y;

            m_midX_B = (P1_X+P2_X+P3_X)/3;
            m_midY_B = (P1_Y+P2_Y+P3_Y)/3;
            m_half_lX_B = ((GetMax(P1_X,P2_X,P3_X)-GetMin(P1_X,P2_X,P3_X))/2.)+2;
            m_half_lY_B = ((GetMax(P1_Y,P2_Y,P3_Y)-GetMin(P1_Y,P2_Y,P3_Y))/2.)+2;
        }
        
        bool IsInside(int x, int y)
        {
            double v2X = x-m_pX;
            double v2Y = y-m_pY;
            std::cout << "v2X: " << v2X << " " << "v2Y " << v2Y << std::endl;      
 
            double v00 = m_v0X*m_v0X + m_v0Y*m_v0Y;
            double v01 = m_v0X*m_v1X + m_v0Y*m_v1Y;
            double v02 = m_v0X*v2X   + m_v0Y*v2Y;
            double v11 = m_v1X*m_v1X + m_v1Y*m_v1Y;
            double v12 = m_v1X*v2X   + m_v1Y*v2Y;
 
            double h1 = (v11*v02-v01*v12)/(v00*v11-v01*v01);
            double h2 = (v00*v12-v01*v02)/(v00*v11-v01*v01);

            if( (h1 >= 0) && (h2>=0) && (h1+h2)<=1 ){
                std::cout << x << " " << y << std::endl;
                return 1;
            } 
            else{
                return 0;
            }

        }
        // Rectangle as Bounding Box
        bool IsBoundingBox(int x, int y)
        {
            int xn = abs(x-m_midX_B);
            int yn = abs(y-m_midY_B);
            if(xn <= m_half_lX_B && yn <= m_half_lY_B){
                return 1;
            }
            else{ return 0; }
        }      

        double GetMin(int p1, int p2, int p3)
        {
            int min = 0;
            if (p1 < p2){
                min = p1;
            }
            else{
                min = p2;
            }

            if(min < p3){
                return min;
            }
            else{ 
                return p3;
            }
        }

        double GetMax( int p1, int p2, int p3)
        {
            int max = 0;
            if(p1 > p2){
                max = p1;
            }
            else{
                max = p2;
            }
     
            if(max > p3){
                return max;
            }
            else{
                return p3;
            }
        }
};

#endif
