#include "ImageReader.h"

// Constructor
ImageReader::ImageReader(string image, int sizex, int sizey ) : m_sizeX(sizex), m_sizeY(sizey)
{
    m_vals = vector<int>(sizex*sizey,255); 
    TestPGM( image );
    ReadPGM( image );
}

// Destructor
ImageReader::~ImageReader()
{

}

//======================================
//****    Util functions            ****
//======================================

void ImageReader::TestPGM( string test )
{ 
    int size = test.size();
    string sub = test.substr(size-3,3);
    if((sub != "PGM") && (sub != "pgm")){
        std::cerr << "No pgm Image " << std::endl; 
        exit(EXIT_FAILURE); 
    }
}
void ImageReader::ReadPGM( string image )
{
    string line;
    stringstream ss(stringstream::in | stringstream::out);
    int val;
    ifstream ifs;

    ifs.open( image.c_str(), ifstream::in);
    getline(ifs,line);
    if(line != "P2"){ std::cerr << "NO ASCII" << std::endl; exit(EXIT_FAILURE); }
    getline(ifs, line);//name 
    getline(ifs, line);//size of image
    ss << line;
    ss >> val;
    if(val != m_sizeX){cerr << "Wrong Picture size X" << endl; exit(EXIT_FAILURE); }
    ss >> val;
    if(val != m_sizeY){cerr << "Wrong Picture size Y" << endl; exit(EXIT_FAILURE); }
    getline(ifs, line);//Grexscale value
    int sizeY = m_sizeY-1;
    for(int i= sizeY; i >=0;--i){
        for( int j = 0; j < m_sizeX; ++j ){
             stringstream ss2(stringstream::in | stringstream::out);
             getline(ifs, line);
             ss2 << line;
             ss2 >> val;
             SetValue(i,j,val);             
        }
    }
    ifs.close();
}
/*
void ImageReader::ReadPGM( string image )
{
    string line;
    stringstream ss;
    int tmp;
    ifstream ifs;
    ifs.open( image.c_str(), ifstream::in );
    //Identifikation ASCII
    getline(ifs,line);
    if(line != "P2"){ std::cerr << "NO ASCII" << std::endl; exit(EXIT_FAILURE); }
    //Dateiname
    getline(ifs,line);
    //Grösse Breite Höhe
    getline(ifs,line);
    ss(line);
    ss >>tmp;
    if(tmp != m_sizeX) std::cerr << "Wrong size X " << std::endl; exit(EXIT_FAILURE); } 
    ss >>tmp;
    if(tmp != m_sizeY) std::cerr << "Wrong size X " << std::endl; exit(1); } 
    //Max Greyscale
    getline(ifs,line);
    //Values    
    while( ifs.good() ){
        getline(ifs,line);
        ss(line);
        for(int i = 0; i < m_sizeX; ++i ){
            ss >> tmp;
            tmp.push_back(tmp);
        }
    }
    ifs.close();
}
*/
void ImageReader::ReadPGM_2( string image )
{
    FILE *file;
    file = fopen(image.c_str(), "r");
    if(file == NULL){std::cerr << "Can't open Image" << std::endl; exit(1);}
    string str;
    char c[80];
    int val;
    str = "P2";
    fscanf(file,"%s",c);
    
    if(strcmp(c, str.c_str() )!= 0){ std::cerr << "No ASCII Image" << std::endl; exit(1);}
    fscanf(file,"%s",c);
    fscanf(file,"%i",&val);
    fscanf(file,"%i",&val);
    int length = m_sizeX*m_sizeY;;
    for(int i = 0; i < length; ++i ){
        fscanf(file,"%d", &val);
        if(feof(file)) break;
        m_vals.push_back(val);
    
    }
    fclose(file);
}

//======================================
//****    Access functions          ****
//======================================

int ImageReader::GetX()
{
    return m_sizeX;
}

int ImageReader::GetY()
{
    return m_sizeY;
}

int ImageReader::GetValue( const int x, const int y)
{
    return m_vals[y*m_sizeX+x];
}

void ImageReader::SetValue( const int i, const int j, const int value )
{
    m_vals[i*m_sizeX+j] = value;
}

vector<int> ImageReader::GetAllValues()
{
    return m_vals;
}

