#ifndef SELECTORDRAG_H_
#define SELECTORDRAG_H_

class DragForceSelector 
{
public:
    typedef double VariableType;

    static std::string varName()
    {
        return "drag force";
    }
    
    static int dataComponents()
    {
        return 3;
    }

    void operator()(const Cell& cell, double *storage)
    {
        storage[0] = cell.dragX;
        storage[1] = cell.dragY;
        storage[2] = cell.dragZ;
    }
};


#endif
