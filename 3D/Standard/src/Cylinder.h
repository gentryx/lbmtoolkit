#ifndef _CYLINDER_H_
#define _CYLINDER_H_

#include "Geometry.h"

class Cylinder : public Geometry
{
    private:
        int m_p2X;
        int m_p2Y;
        int m_p2Z;

        double m_R;
        double m_H;

    public:
       Cylinder(int px, int py, int pz, int p2x, int p2y, int p2z, int r, int h) : Geometry(px,py,pz),m_p2X(p2x),m_p2Y(p2y),m_p2Z(p2z),m_R(r),m_H(h)
        {

        }

        bool IsInside(int x, int y, int z)
        {
            int tmpX1  = m_p2X - m_X;
            int tmpY1  = m_p2Y - m_Y;
            int tmpZ1  = m_p2Z - m_Z;  
            int tmpX2 = x - m_X;
            int tmpY2 = y - m_Y;
            int tmpZ2 = z - m_Z;
            
            double sqH = (m_H-1)*(m_H-1);
            double sqR = m_R*m_R;
            double val = tmpX1*tmpX2 + tmpY1*tmpY2 + tmpZ1*tmpZ2;
            if( (val < 0.0) || (val > sqH) ) return 0;
            else{
                double val2 = (tmpX2*tmpX2 + tmpY2*tmpY2 + tmpZ2*tmpZ2) - val*val/sqH;
                if(val2 > sqR) return 0;
                else { return 1;}
            } 
        }

        bool IsBoundingBox(int x, int y, int z)
        {
            double h = m_H+2;
            double r = m_R+2;
            double sqH = (h-1)*(h-1);
            double sqR = r*r;
            int i = (m_X==m_p2X)?0:-1;  
            int j = (m_Y==m_p2Y)?0:-1;  
            int k = (m_Z==m_p2Z)?0:-1;

            int xbb1 = m_X +i;
            int xbb2 = m_p2X-i;
            int ybb1 = m_Y +j;
            int ybb2 = m_p2Y-j;
            int zbb1 = m_Z +k;
            int zbb2 = m_p2Z-k;
           
            int tmpX1  = xbb2 - xbb1;
            int tmpY1  = ybb2 - ybb1;
            int tmpZ1  = zbb2 - zbb1; 

            int tmpX2 = x - xbb1;
            int tmpY2 = y - ybb1;
            int tmpZ2 = z - zbb1;

            double val = tmpX1*tmpX2 + tmpY1*tmpY2 + tmpZ1*tmpZ2;
            if( (val < 0.0) || (val > sqH) ) return 0;
            else{
                double val2 = (tmpX2*tmpX2 + tmpY2*tmpY2 + tmpZ2*tmpZ2) - val*val/sqH;
                if(val2 > sqR) return 0;
                else { return 1;}
            } 
            

             
        } 

};


#endif
