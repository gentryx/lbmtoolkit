#ifndef _GEOMETRIE_H_
#define _GEOMETRIE_H_

//#include "Point3d.h"

class Geometry
{
    protected:
       int m_X;
       int m_Y;
       int m_Z;
    
    public:
       Geometry(int mx, int my, int mz) : m_X(mx),m_Y(my),m_Z(mz){  }       

       virtual bool IsInside(int x, int y, int z){return 0;}
       virtual bool IsBoundingBox(int x, int y, int z){return 0;}

};

bool CheckPoint(Geometry *g, int x, int y, int z)
{
 return g->IsInside(x,y,z);
}

bool CheckBound(Geometry *g, int x, int y, int z)
{
 return g->IsBoundingBox(x,y,z);
}

#endif
