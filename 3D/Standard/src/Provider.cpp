#include"Provider.h"
#include<string>
#include "Constants.h"

using namespace std;


Provider::Provider()
{
    InitValues();
}

Provider::~Provider()
{
    
}

void Provider::InitValues()
{
    FileReader Reader;
    Reader.read( g_filename );
  
    m_timesteps = Reader.GetValue<int>( "timesteps" );
    m_size[0] = Reader.GetValue<int>( "sizeX" );
    m_size[1] = Reader.GetValue<int>( "sizeY" );
    m_size[2] = Reader.GetValue<int>( "sizeZ" );
	
    m_dt = Reader.GetValue<double>( "dt"   );
    m_dx = Reader.GetValue<double>( "dx"   );
    m_tau = Reader.GetValue<double>( "tau" );
    if( (m_tau < 0.5) || (m_tau > 2.0) ) cerr << "WARNING: Wrong tau: " << m_tau << endl;
    
    m_initPressure  = Reader.GetValue<double>( "initPressure"    );	
    m_initVelocityX = Reader.GetValue<double>( "initVelocityX_L" );
    m_initVelocityY = Reader.GetValue<double>( "initVelocityY_L" ); 
    m_initVelocityZ = Reader.GetValue<double>( "initVelocityZ_L" ); 

    m_gravityX = Reader.GetValue<double>( "GravityX" );
    m_gravityY = Reader.GetValue<double>( "GravityY" );
    m_gravityZ = Reader.GetValue<double>( "GravityZ" );

    m_forceX = Reader.GetValue<double>( "ForceX" );
    m_forceY = Reader.GetValue<double>( "ForceY" );
    m_forceZ = Reader.GetValue<double>( "ForceZ" );

    m_boundaryVals = new map<string,BoundaryStorage>();
 
    BoundaryStorage tmp = Reader.GetStruct("Top");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("Top",tmp));

    tmp = Reader.GetStruct("Bottom");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("Bottom",tmp));

    tmp = Reader.GetStruct("North");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("North",tmp));
    
    tmp = Reader.GetStruct("South");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("South",tmp));
     
    tmp = Reader.GetStruct("East");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("East",tmp));

    tmp = Reader.GetStruct("West");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("West",tmp));

    tmp = Reader.GetStruct("TopWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopWest",tmp));

    tmp = Reader.GetStruct("TopEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopEast",tmp));

    tmp = Reader.GetStruct("TopSouth");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopSouth",tmp));

    tmp = Reader.GetStruct("TopNorth");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopNorth",tmp));

    tmp = Reader.GetStruct("BottomWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomWest",tmp));

    tmp = Reader.GetStruct("BottomEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomEast",tmp));

    tmp = Reader.GetStruct("BottomSouth");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomSouth",tmp));

    tmp = Reader.GetStruct("BottomNorth");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomNorth",tmp));

    tmp = Reader.GetStruct("NorthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("NorthWest",tmp));

    tmp = Reader.GetStruct("NorthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("NorthEast",tmp));

    tmp = Reader.GetStruct("SouthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("SouthWest",tmp));

    tmp = Reader.GetStruct("SouthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("SouthEast",tmp));

    tmp = Reader.GetStruct("BottomSouthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomSouthWest",tmp));

    tmp = Reader.GetStruct("BottomSouthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomSouthEast",tmp));

    tmp = Reader.GetStruct("BottomNorthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomNorthWest",tmp));

    tmp = Reader.GetStruct("BottomNorthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("BottomNorthEast",tmp));

    tmp = Reader.GetStruct("TopNorthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopNorthWest",tmp));

    tmp = Reader.GetStruct("TopNorthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopNorthEast",tmp));

    tmp = Reader.GetStruct("TopSouthWest");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopSouthWest",tmp));

    tmp = Reader.GetStruct("TopSouthEast");
    m_boundaryVals->insert(std::pair<string,BoundaryStorage>("TopSouthEast",tmp));

}

//====================
//   Util Stuff pt 2
//====================
void Provider::CalcNue_L()
{
    m_nueL = (2.0 * m_tau -1.0)/6.0;
}

void Provider::CalcNue_P()
{
    m_nueP = (m_nueL*m_dx*m_dx)/m_dt;
}

//=======================
//    Access Functions
//=======================
double Provider::GetDx()
{
    return m_dx;
}

double Provider::GetDt()
{
    return m_dt;
}

double Provider::GetGravityX()
{
    return m_gravityX;
}

double Provider::GetGravityY()
{
    return m_gravityY;
}

double Provider::GetGravityZ()
{
    return m_gravityZ;
}

double Provider::GetForceX()
{
    return m_forceX;
}

double Provider::GetForceY()
{
    return m_forceY;
}

double Provider::GetForceZ()
{
    return m_forceZ;
}

int Provider::GetSize( char c )
{
    if( c == 'x' ){
        return m_size[0];
    }
    else if( c == 'y' ){
        return m_size[1]; 
    }
    else if( c == 'z' ){
        return m_size[2];
    }
    else{
	std::cerr << "Something strange in the neighborhood! Need help call the Ghostbusters!! " << "#### Wrong call of Provider::GetSize( c ) ### " << std::endl;
	return 10;  
    }	
}

int Provider::GetTimesteps()
{
    return m_timesteps;
}
double Provider::GetTau()
{
    return m_tau;
}

double Provider::GetNue_L()
{
    return m_nueL;
}

double Provider::GetNue_P(){
    return m_nueP;
}

double Provider::GetVelocityX(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.velocityX;

return 0.0;
}

double Provider::GetTypeVelocityX(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.velocityX;     
        }
    }
return 0.0;
}

double Provider::GetVelocityY(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.velocityY;

return 0.0;
}

double Provider::GetTypeVelocityY(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.velocityY;     
        }
    }
return 0.0;
}

double Provider::GetVelocityZ(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.velocityZ;

return 0.0;
}

double Provider::GetTypeVelocityZ(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.velocityZ;     
        }
    }
return 0.0;
}

double Provider::GetPressure(string key)
{

    map<string,BoundaryStorage>::iterator it;
    it = m_boundaryVals->find(key);
    if(it == m_boundaryVals->end()){
        std::cerr << "Provider: --> Wrong Key --> "+key << std::endl;
        exit(1);
    }
    return it->second.pressure;

return 1.0;
}

double Provider::GetTypePressure(string type)
{
    map<string,BoundaryStorage>::iterator it;
    for(it=m_boundaryVals->begin();it != m_boundaryVals->end();++it){
        if(it->second.type == type){
            return it->second.pressure;     
        }
    }
return 1.0;
}

double Provider::GetInitVelocityX()
{
    return m_initVelocityX;
}

double Provider::GetInitVelocityY()
{
    return m_initVelocityY;
}

double Provider::GetInitVelocityZ()
{
    return m_initVelocityZ;
}

double Provider::GetInitPressure()
{
    return m_initPressure;
}

double Provider::GetSimulationTime()
{
    return m_timesteps * m_dt;
}

double Provider::GetSimulationLengthX()
{
    return m_size[0]*m_dx;
}

double Provider::GetSimulationLengthY()
{
    return m_size[1]*m_dx;
}

double Provider::GetSimulationLengthZ()
{
    return m_size[2]*m_dx;
}

double Provider::CalcOmega()
{
    return (1.0)/m_tau;
}

