cmake_minimum_required(VERSION 2.6.4 FATAL_ERROR)
project(LBMToolkit)

find_package(libgeodecomp REQUIRED)
include_directories(${libgeodecomp_INCLUDE_DIRS})

add_subdirectory(2D)
