#!/bin/bash
echo #JOB_ID
echo #QUEUE
#$ -pe openmpi 1 
#$ -cwd
#$ -l exclusive=true
#$ -l h_rt=48:40:00
mpirun.openmpi ./main "../input/params.txt"
