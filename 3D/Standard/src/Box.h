#ifndef _BOX_H_
#define _BOX_H_

#include "Geometry.h"

class Box : public Geometry
{
   private:
       double m_half_lX;
       double m_half_lY;
       double m_half_lZ;
       double m_half_lX_B;
       double m_half_lY_B;
       double m_half_lZ_B;
      
   public:
       Box(int mx, int my, int mz, int lx, int ly, int lz) : Geometry(mx,my,mz)
       {
           m_half_lX = lx/2;
           m_half_lY = ly/2;
           m_half_lZ = lz/2;
           m_half_lX_B = (lx+2)/2;
           m_half_lY_B = (ly+2)/2;
           m_half_lZ_B = (lz+2)/2;
                
       }
       bool IsInside(int x, int y, int z)
       {
           int xn = abs(x-m_X);
           int yn = abs(y-m_Y);
           int zn = abs(z-m_Z);
           if(xn <= m_half_lX && yn <= m_half_lY && zn <= m_half_lZ){
               return 1;
           }
           else{ return 0; }           
       }   
       bool IsBoundingBox(int x, int y, int z)
       {
           int xn = abs(x-m_X);
           int yn = abs(y-m_Y);
           int zn = abs(z-m_Z);
           if(xn <= m_half_lX && yn <= m_half_lY && zn <= m_half_lZ){
               return 1;
           }
           else{ return 0; }           
       }   

};
#endif

