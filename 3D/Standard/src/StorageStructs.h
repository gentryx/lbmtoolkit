#ifndef _STORAGE_STRUCTS_H_
#define _STORAGE_STRUCTS_H_

#include<string>

struct BoundaryStorage{
    std::string type;
    double velocityX;
    double velocityY;
    double velocityZ;
    double pressure;
};

struct CylinderStorage{
    double P1_X;
    double P1_Y;
    double P1_Z;
    double P2_X;
    double P2_Y;
    double P2_Z;
    double h;
    double r;
};

struct BoxStorage{
    double M_X;
    double M_Y;
    double M_Z;
    double l_X;
    double l_Y;
    double l_Z;
};

struct SphereStorage{
    double M_X;
    double M_Y;
    double M_Z;
    double r;
};
#endif

